FROM node:16-alpine3.16 as builder
WORKDIR /app
COPY package*.json ./
RUN npm ci
COPY . .
RUN npm run build --output-path=dist

FROM nginx:1.23.1-alpine
COPY nginx/default.conf /etc/nginx/conf.d/
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /app/dist/ow-manager /usr/share/nginx/html/ow-manager
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
