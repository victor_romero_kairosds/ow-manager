describe('Offensive-Words page (restricted access)', () => {
  beforeEach(() => {
    cy.intercept('POST', 'http://localhost:3000/api/v1/auth/login', {
      fixture: './../../cypress/fixtures/login.json',
    });
    cy.intercept('GET', 'http://localhost:3000/api/v1/auth/role/me', {
      fixture: './../../cypress/fixtures/role-user.json',
    });
    cy.visit('/auth/login');
    cy.findByRole('textbox', {
      name: /email/i,
    }).type('victor.romero@kairosds.com');
    cy.findByLabelText(/password/i).type('1234');
    cy.findByRole('button', {
      name: /login/i,
    }).click();
    cy.wait(200);
    cy.visit('/offensive-word');
  });

  it('cannot access offensive-words page', () => {
    cy.findByRole('link', {
      name: /offensive words/i,
    }).click();
    cy.findAllByText("You don't have permission to access this page.");
  });
});

describe('Offensive-Words page (access allowed)', () => {
  beforeEach(() => {
    cy.intercept('POST', 'http://localhost:3000/api/v1/auth/login', {
      fixture: './../../cypress/fixtures/login.json',
    });
    cy.intercept('GET', 'http://localhost:3000/api/v1/auth/role/me', {
      fixture: './../../cypress/fixtures/role-admin.json',
    });
    cy.intercept('GET', 'http://localhost:3000/api/v1/offensive-word', {
      fixture: './../../cypress/fixtures/offensive-words.json',
    });
    cy.intercept('POST', 'http://localhost:3000/api/v1/offensive-word', {
      fixture: './../../cypress/fixtures/create-offensive-word.json',
    });
    cy.intercept('PUT', 'http://localhost:3000/api/v1/offensive-word/*', {
      fixture: './../../cypress/fixtures/edit-offensive-word.json',
    });
    cy.intercept('DELETE', 'http://localhost:3000/api/v1/offensive-word/*', {
      fixture: './../../cypress/fixtures/delete-offensive-word.json',
    });
    cy.visit('/auth/login');
    cy.findByRole('textbox', {
      name: /email/i,
    }).type('victor.romero@kairosds.com');
    cy.findByLabelText(/password/i).type('1234');
    cy.findByRole('button', {
      name: /login/i,
    }).click();
    cy.wait(200);
    cy.visit('/offensive-word');
  });

  it('can see a list of offensive-words', () => {
    cy.wait(200);
    cy.findByRole('table');
    cy.findAllByRole('row').should('have.length', 3);
    cy.findAllByRole('row')
      .first()
      .children()
      .first()
      .should('have.text', 'Word')
      .next()
      .should('have.text', 'Level')
      .next()
      .should('have.text', 'ID')
      .next()
      .should('have.text', 'Actions');
    cy.findAllByRole('row')
      .last()
      .children()
      .first()
      .should('have.text', 'damn')
      .next()
      .should('have.text', '3')
      .next()
      .next()
      .should('have.text', 'Edit')
      .next()
      .should('have.text', 'Delete');
  });
  it('can update an offensive-word', () => {
    cy.wait(200);
    cy.findAllByRole('row')
      .last()
      .children()
      .first()
      .should('have.text', 'damn')
      .next()
      .should('have.text', '3');
    cy.findAllByRole('button', {
      name: /edit/i,
    })
      .last()
      .click();
    cy.findByRole('textbox', {
      name: /word/i,
    }).clear();
    cy.findAllByText('Word is required and needs to be 2 characters minimum.');
    cy.findByRole('textbox', {
      name: /word/i,
    }).type('word');
    cy.findByRole('radio', {
      name: /level 2/i,
    }).click();
    cy.findByRole('button', {
      name: /save/i,
    }).click();
    cy.wait(200);
    cy.findAllByRole('row')
      .last()
      .children()
      .first()
      .should('have.text', 'word')
      .next()
      .should('have.text', '2');
    cy.findByText(/success/i);
    cy.findByText(/word updated/i);
  });
  it('can delete an offensive-word', () => {
    cy.wait(200);
    cy.findAllByRole('row')
      .last()
      .children()
      .first()
      .should('have.text', 'damn')
      .next()
      .should('have.text', '3');
    cy.findAllByRole('button', {
      name: /delete/i,
    })
      .last()
      .click();
    cy.findByRole('button', {
      name: /confirmar/i,
    }).click();
    cy.findAllByRole('row').should('have.length', 2);
    cy.findByText(/success/i);
    cy.findByText(/word deleted/i);
  });
  it('can create an offensive-word', () => {
    cy.wait(200);
    cy.findAllByRole('row')
      .last()
      .children()
      .first()
      .should('have.text', 'damn')
      .next()
      .should('have.text', '3');
    cy.findAllByRole('button', {
      name: /new word/i,
    })
      .last()
      .click();
    cy.findByRole('textbox', {
      name: /word/i,
    }).type('hello');
    cy.findByRole('radio', {
      name: /level 5/i,
    }).click();
    cy.findByRole('button', {
      name: /save/i,
    }).click();
    cy.findAllByRole('row').should('have.length', 4);
    cy.findAllByRole('row')
      .last()
      .children()
      .first()
      .should('have.text', 'hello')
      .next()
      .should('have.text', '5');
    cy.findByText(/success/i);
    cy.findByText(/word created/i);
  });
});
