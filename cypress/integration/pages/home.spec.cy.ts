describe('Home page', () => {
  it('Displays homepage correctly', () => {
    cy.visit('/');
    cy.contains('Welcome to Offensive Words Manager');
    cy.findByRole('link', {
      name: /log in/i,
    });
  });
});
