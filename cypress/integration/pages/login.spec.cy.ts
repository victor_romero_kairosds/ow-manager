describe('Login page', () => {
  beforeEach(() => {
    cy.intercept('POST', `http://localhost:3000/api/v1/auth/login`, {
      fixture: './../../cypress/fixtures/login.json',
    });
    cy.visit('/auth/login');
  });

  it('Displays login correctly', () => {
    cy.findByRole('textbox', {
      name: /email/i,
    });
    cy.findByLabelText(/password/i);
    cy.findByRole('button', {
      name: /login/i,
    });
  });

  it('Logs in correctly', () => {
    cy.findByRole('button', {
      name: /log out/i,
    }).should('not.exist');
    cy.findByRole('textbox', {
      name: /email/i,
    }).type('victor.romero@kairosds.com');
    cy.findByLabelText(/password/i).type('1234');
    cy.findByRole('button', {
      name: /login/i,
    }).click();
    cy.findByRole('button', {
      name: /log out/i,
    }).should('exist');
  });

  it('Logs out correctly', () => {
    cy.findByRole('textbox', {
      name: /email/i,
    }).type('victor.romero@kairosds.com');
    cy.findByLabelText(/password/i).type('1234');
    cy.findByRole('button', {
      name: /login/i,
    }).click();
    cy.findByRole('link', {
      name: /log in/i,
    }).should('not.exist');
    cy.findByRole('button', {
      name: /log out/i,
    }).click();
    cy.findByRole('link', {
      name: /log in/i,
    }).should('exist');
    cy.findByText(/success/i);
    cy.findByText(/logged in/i);
  });
});
