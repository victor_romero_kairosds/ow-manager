import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import {
  AuthState,
  AuthStoreService,
} from '../../core/auth/services/auth-store.service';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.css'],
})
export class MainLayoutComponent implements OnInit {
  constructor(private store: AuthStoreService, private router: Router) {}

  authState$: Observable<AuthState>;

  ngOnInit(): void {
    this.authState$ = this.store.get$();
  }

  handleLogout() {
    this.store.store('LOGOUT', { token: null, role: null });
    this.router.navigate(['/']);
  }
}
