import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AuthFormComponent } from './auth/components/auth-form/auth-form.component';
import { LoginComponent } from './auth/components/login/login.component';

@NgModule({
  declarations: [LoginComponent, AuthFormComponent],
  imports: [CommonModule, HttpClientModule, ReactiveFormsModule, RouterModule],
})
export class CoreModule {}
