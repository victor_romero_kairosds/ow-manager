import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { NotificationBusService } from '../../../shared/services/notification-bus.service';
import { AuthStoreService } from '../services/auth-store.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(
    private store: AuthStoreService,
    private router: Router,
    private notifier: NotificationBusService
  ) {}

  canActivate(): boolean {
    const token = this.store.get().token;
    if (token) {
      return true;
    }
    this.router.navigate(['/auth/login']);
    this.notifier.showWarn('You need to be logged in to see this page');
    return false;
  }
}
