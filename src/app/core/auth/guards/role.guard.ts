import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';

import { NotificationBusService } from '../../../shared/services/notification-bus.service';
import { AuthStoreService } from '../../auth/services/auth-store.service';

@Injectable({
  providedIn: 'root',
})
export class RoleGuard implements CanActivate {
  constructor(
    private store: AuthStoreService,
    private router: Router,
    private notifier: NotificationBusService
  ) {}

  async canActivate(route: ActivatedRouteSnapshot): Promise<boolean> {
    let store = this.store.get();
    if (!store.role) {
      await this.store.getRole();
    }
    store = this.store.get();
    const role = store.role;
    // eslint-disable-next-line dot-notation
    if (route.data['role'] === role) {
      return true;
    }
    this.router.navigate(['/home']);
    this.notifier.showError("You don't have permission to access this page.");
    return false;
  }
}
