export interface SignUpRequestDTO {
  email: string;
  password: string;
}

export interface UserDTO {
  id: string;
  email: string;
}

export interface AuthTokenDTO {
  user_token: string;
}

export interface LoginRequestDTO {
  email: string;
  password: string;
}

export interface RoleDTO {
  role: string;
}
