import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-auth-form',
  templateUrl: './auth-form.component.html',
  styleUrls: ['./auth-form.component.css'],
})
export class AuthFormComponent implements OnInit {
  authForm: FormGroup;
  @Output() submitData = new EventEmitter();
  @Input() labelText: string;

  ngOnInit(): void {
    this.authForm = new FormGroup({
      email: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    });
  }

  sendCredentials() {
    const credentials = {
      email: this.authForm.get('email')?.value,
      password: this.authForm.get('password')?.value,
    };
    this.submitData.emit(credentials);
  }
}
