import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';

import { AuthFormComponent } from './auth-form.component';

describe('AuthFormComponent', () => {
  let component: AuthFormComponent;
  let fixture: ComponentFixture<AuthFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AuthFormComponent],
      imports: [ReactiveFormsModule],
    }).compileComponents();

    fixture = TestBed.createComponent(AuthFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit an event with credentials when login', () => {
    component.authForm.get('email')?.setValue('vic@test.com');
    component.authForm.get('password')?.setValue('1234');
    let authCredentials = null;
    component.submitData.subscribe(
      (credentials) => (authCredentials = credentials)
    );
    component.sendCredentials();
    expect(authCredentials).not.toBeNull();
    expect(authCredentials).toEqual({
      email: 'vic@test.com',
      password: '1234',
    });
  });
});
