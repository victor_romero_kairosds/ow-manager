import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationBusService } from '../../../../shared/services/notification-bus.service';
import { AuthStoreService } from '../../services/auth-store.service';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  constructor(
    private store: AuthStoreService,
    private router: Router,
    private notifier: NotificationBusService
  ) {}

  async login(credentials: any) {
    await this.store.getToken(credentials.email, credentials.password);
    this.router.navigate(['/home']);
    this.notifier.showSuccess('Logged in');
  }
}
