import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthStoreService } from '../services/auth-store.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private store: AuthStoreService) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    if (
      request.url.includes('/auth/login') ||
      request.url.includes('/auth/sign-up')
    ) {
      return next.handle(request);
    }
    const authToken = this.store.get();

    const authReq = request.clone({
      headers: request.headers.set(
        'Authorization',
        `Bearer ${authToken?.token}`
      ),
    });

    return next.handle(authReq);
  }
}
