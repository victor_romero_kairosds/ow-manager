import { Injectable } from '@angular/core';

import { fromEvent, lastValueFrom, tap } from 'rxjs';
import { Store } from '../../../shared/state/store';
import { AuthTokenDTO, RoleDTO, UserDTO } from '../model/auth.model';
import { AuthRepositoryService } from './auth-repository.service';

export interface AuthState {
  token: string | null;
  role: string | null;
}

@Injectable({
  providedIn: 'root',
})
export class AuthStoreService extends Store<AuthState> {
  constructor(private repository: AuthRepositoryService) {
    super();
    fromEvent(window, 'beforeunload').subscribe(() =>
      this.saveToLocalStorage(`POSTS_API`)
    );
    this.loadFromLocalStorage('POSTS_API');
  }

  init() {
    this.store('AUTH_STORE_INIT', { token: null, role: null });
  }

  saveToLocalStorage = (keyName: string) => {
    const state = this.get();
    if (state) {
      localStorage.setItem(keyName, JSON.stringify({ token: state.token }));
    }
  };

  loadFromLocalStorage = (keyName: string) => {
    const token = localStorage.getItem(keyName);
    if (token) {
      const tokenObj = JSON.parse(token);
      this.store('LOAD_STATE', { token: tokenObj.token, role: null });
      localStorage.removeItem(keyName);
    }
  };

  getToken(email: string, password: string): Promise<AuthTokenDTO> {
    return lastValueFrom(
      this.repository.login({ email, password }).pipe(
        tap((token: AuthTokenDTO) => {
          this.store('GET_TOKEN', { ...this.get(), token: token.user_token });
        })
      )
    );
  }

  signUp(email: string, password: string): Promise<UserDTO> {
    return lastValueFrom(
      this.repository.signUp({ email, password }).pipe(
        tap(() => {
          this.getToken(email, password);
        })
      )
    );
  }

  getRole(): Promise<RoleDTO> {
    return lastValueFrom(
      this.repository.getRole().pipe(
        tap((role: RoleDTO) => {
          this.store('GET_ROLE', { ...this.get(), role: role.role });
        })
      )
    );
  }
}
