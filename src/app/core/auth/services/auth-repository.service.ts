import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ConfigService } from '../../../config/config.service';

import {
  AuthTokenDTO,
  LoginRequestDTO,
  RoleDTO,
  SignUpRequestDTO,
  UserDTO,
} from '../model/auth.model';

@Injectable({
  providedIn: 'root',
})
export class AuthRepositoryService {
  constructor(private http: HttpClient, private env: ConfigService) {}

  signUp(request: SignUpRequestDTO): Observable<UserDTO> {
    return this.http.post<UserDTO>(
      `${this.env.config.API_URL}/auth/sign-up`,
      request
    );
  }

  login(request: LoginRequestDTO): Observable<AuthTokenDTO> {
    return this.http.post<AuthTokenDTO>(
      `${this.env.config.API_URL}/auth/login`,
      request
    );
  }

  getRole(): Observable<RoleDTO> {
    return this.http.get<RoleDTO>(`${this.env.config.API_URL}/auth/role/me`);
  }
}
