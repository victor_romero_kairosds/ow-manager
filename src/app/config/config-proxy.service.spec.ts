import { HttpClientTestingModule } from '@angular/common/http/testing';

import { inject, TestBed } from '@angular/core/testing';
import { ConfigProxyService } from './config-proxy.service';

describe('ConfigProxyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
  });
  it('should be created', inject(
    [ConfigProxyService],
    (service: ConfigProxyService) => {
      expect(service).toBeTruthy();
    }
  ));
});
