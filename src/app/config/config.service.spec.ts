import { inject, TestBed } from '@angular/core/testing';

import { ConfigProxyService } from './config-proxy.service';
import { ConfigProxyServiceFake } from './config-proxy.service.fake';
import { ConfigService } from './config.service';

describe('ConfigService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ConfigService,
        { provide: ConfigProxyService, useClass: ConfigProxyServiceFake },
      ],
    });
  });
  it('should be created', inject([ConfigService], (service: ConfigService) => {
    expect(service).toBeTruthy();
  }));

  it('should load configuration', () => {
    const service: ConfigService = TestBed.get(ConfigService);
    service.load();
    expect(service.config.API_URL).toBe('http://localhost:3000/api/v1');
  });
});
