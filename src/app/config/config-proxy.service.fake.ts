import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { of } from 'rxjs/internal/observable/of';
import { Config } from './config';

const CONFIG_FAKE: Config = {
  API_URL: 'http://localhost:3000/api/v1',
};

@Injectable()
export class ConfigProxyServiceFake {
  getConfig(): Observable<Config> {
    return of(CONFIG_FAKE);
  }
}
