import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';

import { INotification } from '../../../../shared/model/notification.type';
import { NotificationBusService } from '../../../../shared/services/notification-bus.service';
import { OffensiveWord } from '../../model/model';
import { OffensiveWordStoreService } from '../../services/offensive-word-store.service';

@Component({
  selector: 'app-offensive-word-list',
  templateUrl: './offensive-word-list.component.html',
  styleUrls: ['./offensive-word-list.component.css'],
})
export class OffensiveWordListComponent implements OnInit {
  @ViewChild('createModal') public createModal: ElementRef;
  @ViewChild('editModal') public editModal: ElementRef;
  @ViewChild('deleteModal') public deleteModal: ElementRef;

  listOfOffensiveWords$: Observable<OffensiveWord[]>;
  msgs: INotification[];
  currentWord: OffensiveWord | null;

  constructor(
    private store: OffensiveWordStoreService,
    private notifierBus: NotificationBusService
  ) {}

  ngOnInit(): void {
    this.listOfOffensiveWords$ = this.store.get$();
    this.store.getAllOffensiveWords();
    this.notifierBus
      .getNotification()
      .subscribe((notification: INotification) => {
        this.msgs = [];
        this.msgs.push(notification);
      });
  }

  openCreateModal() {
    this.createModal.nativeElement.showModal();
  }

  hideCreateModal() {
    this.createModal.nativeElement.close();
  }

  async onCreate(word: OffensiveWord): Promise<void> {
    try {
      await this.store.createOffensiveWord(word);
      this.hideCreateModal();
      this.notifierBus.showSuccess(`Word created`);
    } catch (e) {
      console.log(e);
    }
  }

  async onUpdate(wordToUpdate: OffensiveWord): Promise<void> {
    try {
      const { id } = wordToUpdate;
      await this.store.updateOffensiveWord(id, wordToUpdate);
      this.hideUpdateModal();
      this.notifierBus.showSuccess(`Word updated`);
    } catch (e) {
      console.log(e);
    }
  }

  openUpdateModal(word: OffensiveWord) {
    this.currentWord = { ...word };
    this.editModal.nativeElement.showModal();
  }

  hideUpdateModal() {
    this.editModal.nativeElement.close();
  }

  async onDelete(id: string): Promise<void> {
    try {
      await this.store.deleteOffensiveWord(id);
      this.hideDeleteModal();
      this.notifierBus.showSuccess(`Word deleted`);
    } catch (e) {
      console.log(e);
    }
  }

  hideDeleteModal() {
    this.deleteModal.nativeElement.close();
  }

  openDeleteModal(word: OffensiveWord) {
    this.currentWord = { ...word };
    this.deleteModal.nativeElement.showModal();
  }
}
