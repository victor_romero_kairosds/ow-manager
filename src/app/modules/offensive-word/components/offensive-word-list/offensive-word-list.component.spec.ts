import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { of } from 'rxjs';
import { OffensiveWord } from '../../model/model';

import { OffensiveWordRepositoryService } from '../../services/offensive-word-repository.service';
import { OffensiveWordProxyFakeService } from '../../services/offensive-word-repository.service.fake';
import { OffensiveWordConfirmFormComponent } from '../offensive-word-confirm-form/offensive-word-confirm-form.component';
import { OffensiveWordFormComponent } from '../offensive-word-form/offensive-word-form.component';
import { OffensiveWordListComponent } from './offensive-word-list.component';

export const LIST_OF_OW = {
  results: [
    {
      id: '7d48b633-e523-4431-94dc-84d3395e3bfc',
      word: 'bullshit',
      level: 5,
    },
    {
      id: '3e27040d-e875-45b4-9946-ca86721416f4',
      word: 'damn',
      level: 2,
    },
  ],
  totalResults: 2,
};

export const OW = {
  id: '4c277f17-cc1c-4fe8-84af-4949ac112b7e',
  word: 'fck',
  level: 5,
};

export const UPDATED_OW = {
  id: '7d48b633-e523-4431-94dc-84d3395e3bfc',
  level: 2,
  word: 'hello',
};

describe('OffensiveWordListComponent', () => {
  let component: OffensiveWordListComponent;
  let fixture: ComponentFixture<OffensiveWordListComponent>;
  let proxy: OffensiveWordRepositoryService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        OffensiveWordListComponent,
        OffensiveWordFormComponent,
        OffensiveWordConfirmFormComponent,
      ],
      imports: [HttpClientTestingModule, ReactiveFormsModule],
      providers: [
        {
          provide: OffensiveWordRepositoryService,
          useClass: OffensiveWordProxyFakeService,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(OffensiveWordListComponent);
    component = fixture.componentInstance;
    proxy = TestBed.inject(OffensiveWordRepositoryService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load a list of ow', waitForAsync(() => {
    const spyOnGet = jest
      .spyOn(proxy, 'getAllOffensiveWords')
      .mockImplementation(() => of(LIST_OF_OW));

    component.ngOnInit();

    component.listOfOffensiveWords$.subscribe((ow: OffensiveWord[]) => {
      expect(ow).toHaveLength(2);
      expect(ow).toEqual(LIST_OF_OW.results);
    });
  }));

  it('should add an ow', waitForAsync(() => {
    const spyOnGet = jest
      .spyOn(proxy, 'getAllOffensiveWords')
      .mockImplementation(() => of(LIST_OF_OW));

    component.ngOnInit();

    const spyOnAdd = jest
      .spyOn(proxy, 'createOffensiveWord')
      .mockImplementation(() => of(OW));

    const mockCloseModal = jest
      .spyOn(component, 'hideCreateModal')
      .mockImplementation(() => {});

    component.onCreate(OW);

    component.listOfOffensiveWords$.subscribe((ow: OffensiveWord[]) => {
      expect(ow).toHaveLength(3);
      expect(ow).toEqual([
        {
          id: '7d48b633-e523-4431-94dc-84d3395e3bfc',
          level: 5,
          word: 'bullshit',
        },
        { id: '3e27040d-e875-45b4-9946-ca86721416f4', level: 2, word: 'damn' },
        { id: '4c277f17-cc1c-4fe8-84af-4949ac112b7e', level: 5, word: 'fck' },
      ]);
    });
  }));

  it('should update an ow', waitForAsync(() => {
    const spyOnGet = jest
      .spyOn(proxy, 'getAllOffensiveWords')
      .mockImplementation(() => of(LIST_OF_OW));

    component.ngOnInit();

    const mockCloseModal = jest
      .spyOn(component, 'hideUpdateModal')
      .mockImplementation(() => {});

    component.onUpdate(UPDATED_OW);

    component.listOfOffensiveWords$.subscribe((ow: OffensiveWord[]) => {
      expect(ow).toHaveLength(2);
      expect(ow).toEqual([
        {
          id: '7d48b633-e523-4431-94dc-84d3395e3bfc',
          level: 2,
          word: 'hello',
        },
        { id: '3e27040d-e875-45b4-9946-ca86721416f4', level: 2, word: 'damn' },
      ]);
    });
  }));

  it('should delete an ow', waitForAsync(() => {
    const spyOnGet = jest
      .spyOn(proxy, 'getAllOffensiveWords')
      .mockImplementation(() => of(LIST_OF_OW));

    component.ngOnInit();

    const mockCloseModal = jest
      .spyOn(component, 'hideDeleteModal')
      .mockImplementation(() => {});

    component.onDelete(LIST_OF_OW.results[0].id);

    component.listOfOffensiveWords$.subscribe((ow: OffensiveWord[]) => {
      expect(ow).toHaveLength(1);
      expect(ow).toEqual([
        { id: '3e27040d-e875-45b4-9946-ca86721416f4', level: 2, word: 'damn' },
      ]);
    });
  }));
});
