import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { OffensiveWord } from '../../model/model';

@Component({
  selector: 'app-offensive-word-confirm-form',
  templateUrl: './offensive-word-confirm-form.component.html',
  styleUrls: ['./offensive-word-confirm-form.component.css'],
})
export class OffensiveWordConfirmFormComponent implements OnInit, OnChanges {
  @Output() submitData = new EventEmitter();
  @Output() cancelForm = new EventEmitter();
  @Input() ow: OffensiveWord | null;
  form: FormGroup;

  ngOnInit(): void {
    this.form = new FormGroup({
      id: new FormControl(this.ow?.id || ''),
    });
  }

  ngOnChanges(): void {
    if (this.ow && this.form) {
      this.form.setValue({
        id: this.ow.id,
      });
    }
  }

  onCancel(): void {
    this.cancelForm.emit();
  }

  onSubmit(): void {
    this.submitData.emit(this.form.get('id')?.value);
    this.form.reset();
  }
}
