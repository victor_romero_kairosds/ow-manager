import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OffensiveWordConfirmFormComponent } from './offensive-word-confirm-form.component';

describe('OffensiveWordConfirmFormComponent', () => {
  let component: OffensiveWordConfirmFormComponent;
  let fixture: ComponentFixture<OffensiveWordConfirmFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OffensiveWordConfirmFormComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OffensiveWordConfirmFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
