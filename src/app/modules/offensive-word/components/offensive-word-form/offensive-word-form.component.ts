import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { OffensiveWord } from '../../model/model';

@Component({
  selector: 'app-offensive-word-form',
  templateUrl: './offensive-word-form.component.html',
  styleUrls: ['./offensive-word-form.component.css'],
})
export class OffensiveWordFormComponent implements OnInit, OnChanges {
  @Output() sendWord = new EventEmitter();
  @Output() closeForm = new EventEmitter();
  @Input() ow: OffensiveWord | null;
  form: FormGroup;

  ngOnInit(): void {
    this.form = new FormGroup({
      id: new FormControl(this.ow?.id || ''),
      word: new FormControl(this.ow?.word || '', [
        Validators.required,
        Validators.minLength(2),
      ]),
      level: new FormControl(JSON.stringify(this.ow?.level || ''), [
        Validators.required,
        Validators.min(1),
        Validators.max(5),
      ]),
    });
  }

  ngOnChanges(): void {
    if (this.ow && this.form) {
      this.form.setValue({
        id: this.ow.id,
        word: this.ow.word,
        level: JSON.stringify(this.ow.level),
      });
    }
  }

  onCancel(): void {
    this.closeForm.emit();
    this.form.reset();
  }

  onSubmit(): void {
    if (this.form.status === 'VALID') {
      const offensiveWord: OffensiveWord = {
        id: this.form.get('id')?.value,
        word: this.form.get('word')?.value,
        level: +this.form.get('level')?.value,
      };
      this.sendWord.emit(offensiveWord);
      this.form.reset();
    }
  }

  get id() {
    return this.form.get('id');
  }

  get word() {
    return this.form.get('word');
  }

  get level() {
    return this.form.get('level');
  }
}
