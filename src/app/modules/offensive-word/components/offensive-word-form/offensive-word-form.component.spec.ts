import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';

import { OffensiveWordFormComponent } from './offensive-word-form.component';
describe('OffensiveWordFormComponent', () => {
  let component: OffensiveWordFormComponent;
  let fixture: ComponentFixture<OffensiveWordFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OffensiveWordFormComponent],
      imports: [ReactiveFormsModule],
    }).compileComponents();

    fixture = TestBed.createComponent(OffensiveWordFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit an event with offensive word data whe creating', () => {
    component.form.get('word')?.setValue('badword');
    component.form.get('level')?.setValue('3');
    let ow = null;
    component.sendWord.subscribe((word) => (ow = word));
    component.onSubmit();
    expect(ow).not.toBeNull();
    expect(ow).toEqual({ id: '', level: 3, word: 'badword' });
  });

  it('should emit an event when closing the form and reset the form', () => {
    let ow;
    component.closeForm.subscribe(() => (ow = 'emitted'));
    component.onCancel();
    expect(ow).toBe('emitted');
    expect(component.form.get('word')?.value).toBe(null);
    expect(component.form.get('level')?.value).toBe(null);
  });
});
