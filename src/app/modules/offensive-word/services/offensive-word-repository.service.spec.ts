import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { OffensiveWordRepositoryService } from './offensive-word-repository.service';

describe('OffensiveWordRespositoryService', () => {
  let service: OffensiveWordRepositoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(OffensiveWordRepositoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
