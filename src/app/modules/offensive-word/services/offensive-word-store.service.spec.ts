import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { OffensiveWordStoreService } from './offensive-word-store.service';

describe('OffensiveWordStoreService', () => {
  let service: OffensiveWordStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(OffensiveWordStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
