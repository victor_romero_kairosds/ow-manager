import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { OffensiveWordDTO, ResultDTO } from '../model/dto';
import { OffensiveWord } from '../model/model';
import { OffensiveWordRepositoryService } from './offensive-word-repository.service';

@Injectable({
  providedIn: 'root',
})
export class OffensiveWordService {
  constructor(private repository: OffensiveWordRepositoryService) {}

  getAllOffensiveWords(): Observable<OffensiveWord[]> {
    return this.repository.getAllOffensiveWords().pipe(
      map((results: ResultDTO) => {
        return results.results.map(this.dtoToModel);
      })
    );
  }

  getOneOffensiveWord(id: string): Observable<OffensiveWord> {
    return this.repository.getOneOffensiveWord(id).pipe(map(this.dtoToModel));
  }

  createOffensiveWord(word: OffensiveWord): Observable<OffensiveWord> {
    return this.repository.createOffensiveWord(word).pipe(map(this.dtoToModel));
  }

  updateOffensiveWord(
    id: string,
    word: OffensiveWord
  ): Observable<OffensiveWord> {
    return this.repository
      .updateOffensiveWord(id, { word: word.word, level: word.level })
      .pipe(map(this.dtoToModel));
  }

  deleteOffensiveWord(id: string): Observable<void> {
    return this.repository.deleteOffensiveWord(id);
  }

  dtoToModel(dto: OffensiveWordDTO): OffensiveWord {
    return {
      id: dto.id,
      word: dto.word,
      level: dto.level,
    };
  }
}
