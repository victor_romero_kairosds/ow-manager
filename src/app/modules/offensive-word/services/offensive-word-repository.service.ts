import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ConfigService } from '../../../config/config.service';
import {
  IdRequestDTO,
  OffensiveWordDTO,
  OffensiveWordRequestDTO,
  ResultDTO,
} from '../model/dto';

@Injectable({
  providedIn: 'root',
})
export class OffensiveWordRepositoryService {
  constructor(private http: HttpClient, private env: ConfigService) {}

  getAllOffensiveWords(): Observable<ResultDTO> {
    return this.http.get<ResultDTO>(
      `${this.env.config.API_URL}/offensive-word`
    );
  }

  getOneOffensiveWord(id: IdRequestDTO): Observable<OffensiveWordDTO> {
    return this.http.get<OffensiveWordDTO>(
      `${this.env.config.API_URL}/offensive-word/${id}`
    );
  }

  createOffensiveWord(
    word: OffensiveWordRequestDTO
  ): Observable<OffensiveWordDTO> {
    return this.http.post<OffensiveWordDTO>(
      `${this.env.config.API_URL}/offensive-word`,
      word
    );
  }

  updateOffensiveWord(
    id: IdRequestDTO,
    word: OffensiveWordRequestDTO
  ): Observable<OffensiveWordDTO> {
    return this.http.put<OffensiveWordDTO>(
      `${this.env.config.API_URL}/offensive-word/${id}`,
      word
    );
  }

  deleteOffensiveWord(id: IdRequestDTO): Observable<void> {
    return this.http.delete<void>(
      `${this.env.config.API_URL}/offensive-word/${id}`
    );
  }
}
