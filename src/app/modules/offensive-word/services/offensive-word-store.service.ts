import { Injectable } from '@angular/core';
import { lastValueFrom, tap } from 'rxjs';

import { Store } from '../../../shared/state/store';
import { OffensiveWord } from '../model/model';
import { OffensiveWordService } from '../services/offensive-word.service';

@Injectable({
  providedIn: 'root',
})
export class OffensiveWordStoreService extends Store<OffensiveWord[]> {
  constructor(private offensiveWordService: OffensiveWordService) {
    super();
  }

  init() {
    this.store('AUTH_STORE_INIT', []);
  }

  getAllOffensiveWords(): Promise<OffensiveWord[]> {
    return lastValueFrom(
      this.offensiveWordService
        .getAllOffensiveWords()
        .pipe(
          tap((offensiveWords) =>
            this.store('GET_ALL_OFFENSIVE_WORDS', offensiveWords)
          )
        )
    );
  }

  createOffensiveWord(word: OffensiveWord): Promise<OffensiveWord> {
    return lastValueFrom(
      this.offensiveWordService
        .createOffensiveWord(word)
        .pipe(
          tap((offensiveWord) =>
            this.store('CREATE_OFFENSIVE_WORD', [...this.get(), offensiveWord])
          )
        )
    );
  }

  updateOffensiveWord(id: string, word: OffensiveWord): Promise<OffensiveWord> {
    return lastValueFrom(
      this.offensiveWordService.updateOffensiveWord(id, word).pipe(
        tap((updatedWord) => {
          return this.store(
            'UPDATE_OFFENSIVE_WORD',
            this.get().map((ow) => {
              return ow.id === updatedWord.id ? updatedWord : ow;
            })
          );
        })
      )
    );
  }

  deleteOffensiveWord(id: string): Promise<void> {
    return lastValueFrom(
      this.offensiveWordService.deleteOffensiveWord(id).pipe(
        tap(() =>
          this.store(
            'DELETE_OFFENSIVE_WORD',
            this.get().filter((ow) => ow.id !== id)
          )
        )
      )
    );
  }
}
