import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { OffensiveWordService } from './offensive-word.service';

describe('OffensiveWordService', () => {
  let service: OffensiveWordService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(OffensiveWordService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
