import { Observable, of } from 'rxjs';
import {
  IdRequestDTO,
  OffensiveWordDTO,
  OffensiveWordRequestDTO,
  ResultDTO,
} from '../model/dto';

export const LIST_OF_OW = {
  results: [
    {
      id: '7d48b633-e523-4431-94dc-84d3395e3bfc',
      word: 'bullshit',
      level: 5,
    },
    {
      id: '3e27040d-e875-45b4-9946-ca86721416f4',
      word: 'damn',
      level: 2,
    },
  ],
  totalResults: 2,
};

export const OW = {
  id: '4c277f17-cc1c-4fe8-84af-4949ac112b7e',
  word: 'fck',
  level: 5,
};

export const UPDATED_OW = {
  id: '7d48b633-e523-4431-94dc-84d3395e3bfc',
  level: 2,
  word: 'hello',
};

export class OffensiveWordProxyFakeService {
  constructor() {}

  getAllOffensiveWords(): Observable<ResultDTO> {
    return of(LIST_OF_OW);
  }

  getOneOffensiveWord(id: IdRequestDTO): Observable<OffensiveWordDTO> {
    return of(OW);
  }

  createOffensiveWord(
    word: OffensiveWordRequestDTO
  ): Observable<OffensiveWordDTO> {
    return of(OW);
  }

  updateOffensiveWord(
    id: IdRequestDTO,
    word: OffensiveWordRequestDTO
  ): Observable<OffensiveWordDTO> {
    return of(UPDATED_OW);
  }

  deleteOffensiveWord(id: IdRequestDTO): Observable<{}> {
    return of({});
  }
}
