export interface OffensiveWordDTO {
  id: string;
  word: string;
  level: number;
}

export interface OffensiveWordRequestDTO {
  word: string;
  level: number;
}

export type IdRequestDTO = string;

export interface ResultDTO {
  results: OffensiveWordDTO[];
  totalResults: number;
}
