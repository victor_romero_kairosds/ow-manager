export interface OffensiveWord {
  id: string;
  word: string;
  level: number;
}
