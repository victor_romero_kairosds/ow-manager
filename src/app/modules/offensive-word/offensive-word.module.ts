import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { OffensiveWordConfirmFormComponent } from './components/offensive-word-confirm-form/offensive-word-confirm-form.component';
import { OffensiveWordFormComponent } from './components/offensive-word-form/offensive-word-form.component';
import { OffensiveWordListComponent } from './components/offensive-word-list/offensive-word-list.component';

const ROUTES: Routes = [
  { path: '', component: OffensiveWordListComponent, pathMatch: 'full' },
];

@NgModule({
  declarations: [
    OffensiveWordListComponent,
    OffensiveWordFormComponent,
    OffensiveWordConfirmFormComponent,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(ROUTES),
  ],
  exports: [OffensiveWordListComponent, OffensiveWordFormComponent],
})
export class OffensiveWordModule {}
