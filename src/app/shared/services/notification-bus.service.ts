import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';

import {
  INotification,
  NotificationSeverity,
} from '../model/notification.type';

@Injectable({
  providedIn: 'root',
})
export class NotificationBusService {
  showNotificationSource: ReplaySubject<INotification>;

  constructor() {
    this.showNotificationSource = new ReplaySubject<INotification>();
  }

  getNotification(): Observable<INotification> {
    return this.showNotificationSource.asObservable();
  }

  showError(msg: string) {
    this.show(NotificationSeverity.Error, msg);
  }

  showSuccess(msg: string) {
    this.show(NotificationSeverity.Success, msg);
  }

  showInfo(msg: string) {
    this.show(NotificationSeverity.Info, msg);
  }

  showWarn(msg: string) {
    this.show(NotificationSeverity.Warning, msg);
  }

  private show(severity: NotificationSeverity, msg: string) {
    const notification: INotification = {
      severity,
      msg,
    };
    this.notify(notification);
  }

  private notify(notification: INotification) {
    this.showNotificationSource.next(notification);
  }
}
