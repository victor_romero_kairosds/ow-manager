export enum NotificationSeverity {
  Success = 0,
  Info = 1,
  Warning = 2,
  Error = 3,
}

export interface INotification {
  severity: NotificationSeverity;
  msg: string;
}
