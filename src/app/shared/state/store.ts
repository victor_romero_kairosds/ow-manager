import { BehaviorSubject, Observable } from 'rxjs';

const win = window as any;

export abstract class Store<T> {
  private state$: BehaviorSubject<T> = new BehaviorSubject<T>(
    [] as unknown as T
  );

  constructor() {
    this.init();
  }

  get = (): T => this.state$.getValue();

  get$ = (): Observable<T> => this.state$.asObservable();

  store = (type: string, nextState: T) => {
    if (win.__REDUX_DEVTOOLS_EXTENSION__) {
      win.__REDUX_DEVTOOLS_EXTENSION__.send(type, nextState);
    }
    this.state$.next(nextState);
  };

  abstract init(): void;
}
