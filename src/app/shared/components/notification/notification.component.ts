import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild,
} from '@angular/core';

import { takeWhile } from 'rxjs';
import {
  INotification,
  NotificationSeverity,
} from '../../model/notification.type';
import { NotificationBusService } from '../../services/notification-bus.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css'],
})
export class NotificationComponent implements OnInit, OnDestroy {
  @ViewChild('notificationContainer') container: ElementRef<HTMLDivElement>;
  private _subscribed: boolean = true;
  private classMap: Map<NotificationSeverity, string>;

  constructor(
    private service: NotificationBusService,
    private renderer: Renderer2
  ) {}

  ngOnInit(): void {
    this.classMap = new Map<NotificationSeverity, string>();
    this.classMap.set(NotificationSeverity.Success, 'success');
    this.classMap.set(NotificationSeverity.Info, 'info');
    this.classMap.set(NotificationSeverity.Warning, 'warning');
    this.classMap.set(NotificationSeverity.Error, 'error');

    this.service
      .getNotification()
      .pipe(takeWhile(() => this._subscribed))
      .subscribe((notification) => {
        if (notification) this.render(notification);
      });
  }

  ngOnDestroy() {
    this._subscribed = false;
  }

  private render(notification: INotification) {
    const notificationBox = this.renderer.createElement('div');
    const header = this.renderer.createElement('h1');
    const content = this.renderer.createElement('div');
    const boxColor = this.classMap.get(notification.severity) || 'success';
    this.renderer.addClass(notificationBox, 'message-box');
    this.renderer.addClass(notificationBox, boxColor);
    this.renderer.setStyle(notificationBox, 'transition', `opacity 500ms`);
    this.renderer.setStyle(notificationBox, 'opacity', '1');
    this.renderer.addClass(header, 'header');

    const headerText = this.renderer.createText(
      NotificationSeverity[notification.severity]
    );
    this.renderer.appendChild(header, headerText);
    const text = this.renderer.createText(notification.msg);
    this.renderer.appendChild(content, text);
    this.renderer.appendChild(this.container.nativeElement, notificationBox);
    this.renderer.appendChild(notificationBox, header);
    this.renderer.appendChild(notificationBox, content);
    setTimeout(() => {
      this.renderer.setStyle(notificationBox, 'opacity', '0');
      setTimeout(() => {
        this.renderer.removeChild(
          this.container.nativeElement,
          notificationBox
        );
      }, 2500);
    }, 2500);
  }
}
